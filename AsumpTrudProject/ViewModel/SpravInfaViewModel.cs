﻿using AsumpTrudProject.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsumpTrudProject.ViewModel
{
    class SpravInfaViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        public SpravInfaViewModel(IModule spravInfa)
        {
            if (spravInfa == null) throw new ArgumentNullException(nameof(spravInfa));
            _SpravInfaModule = spravInfa;
        }

        public IModule _SpravInfaModule;
    }
}
