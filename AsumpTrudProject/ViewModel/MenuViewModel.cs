﻿using AsumpTrudProject.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsumpTrudProject.ViewModel
{
    class MenuViewModel
    {
        public MenuViewModel()
        {

        }

        public IModule CodeBehind { get; set; }


        /// <summary>
        /// Переход к вьюшке входная инфа
        /// </summary>
        private RelayCommand _LoadVhodInfaCommand;
        public RelayCommand LoadVhodInfaCommand
        {
            get
            {
                return _LoadVhodInfaCommand = _LoadVhodInfaCommand ??
                  new RelayCommand(OnLoadVhodInfa, CanLoadVhodInfa);
            }
        }
        private bool CanLoadVhodInfa()
        {
            return true;
        }
        private void OnLoadVhodInfa()
        {
            CodeBehind.LoadView(ViewType.VhodInf);
        }


        /// <summary>
        ///переход к вьюшке Справочная информация
        /// </summary>
        private RelayCommand _LoadSpravInfaCommand;
        public RelayCommand LoadSpravInfaCommand
        {
            get
            {
                return _LoadSpravInfaCommand = _LoadSpravInfaCommand ??
                new RelayCommand(OnLoadSpravInfa, CanLoadSpravInfa);
            }
        }

        private bool CanLoadSpravInfa()
        {
            return true;
        }
        private void OnLoadSpravInfa()
        {
            CodeBehind.LoadView(ViewType.SpravInf);
        }


        /// <summary>
        /// переход к вьюшке расчеты
        /// </summary>

        private RelayCommand _LoadCalculateCommand;
        public RelayCommand LoadCalculateCommand
        {
            get
            {
                return _LoadCalculateCommand = _LoadCalculateCommand ??
                 new RelayCommand(OnLoadCalculate,CanLoadCalculate);
            }
        }

        private bool CanLoadCalculate()
        {
            return true;
        }

        private void OnLoadCalculate()
        {
           CodeBehind.LoadView(ViewType.Calculate);
        }

        /// <summary>
        /// Переход к вьюшке просмотр данных
        /// </summary>


        private RelayCommand _LoadProsmDataCommand;
        public RelayCommand LoadProsmDataCommand
        {
            get
            {
                return _LoadProsmDataCommand = _LoadProsmDataCommand ??
                new RelayCommand(OnLoadProsmData, CanLoadProsmData);
            }
        }

        private bool CanLoadProsmData()
        {
            return true;
        }
        private void OnLoadProsmData()
        {
            CodeBehind.LoadView(ViewType.ProsmData);
        }

        /// <summary>
        /// Возвращение к главной вьюшке
        /// </summary>
        private RelayCommand _LoadHomeCommand;
        public RelayCommand LoadHomeCommand
        {
            get
            {
                return _LoadHomeCommand = _LoadHomeCommand ??
                  new RelayCommand(OnLoadHome, CanLoadHome);
            }
        }
        private bool CanLoadHome()
        {
            return true;
        }
        private void OnLoadHome()
        {
            CodeBehind.LoadView(ViewType.Home);
        }

         
    }
}


