﻿using AsumpTrudProject.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace AsumpTrudProject.ViewModel
{
   public class HomeWindowViewModel : INotifyPropertyChanged 
    {
        
            public event PropertyChangedEventHandler PropertyChanged = delegate { };

            //Fields
            private IModule _MainModule;

            //ctor
            public HomeWindowViewModel(IModule codeBehind)
            {
                if (codeBehind == null) throw new ArgumentNullException(nameof(codeBehind));

                _MainModule = codeBehind;
            }

            //Properties

            //Commands

            /// <summary>
            /// Сообщение пользователю
            /// </summary>
            private RelayCommand _ShowMessageCommand;
            public RelayCommand ShowMessageCommand
            {
                get
                {
                    return _ShowMessageCommand = _ShowMessageCommand ??
                      new RelayCommand(OnShowMessage, CanShowMessage);
                }
            }
            private bool CanShowMessage()
            {
                return true;
            }
            private void OnShowMessage()
            {
                _MainModule.ShowMessage("Привет от MainUC");
            }



        }



    }

