﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using AsumpTrudProject.Utils;

namespace AsumpTrudProject.ViewModel
{
    class ProsmDataViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        /// <summary>
        /// передать имя объекта, чтобы указать, какое свойство изменилось.
        /// </summary>
        /// /// <param name="prosm">параметр (принимающий сслыку на объект Module) для проверки на изменение свойства зависимостей </param>
        public ProsmDataViewModel(IModule prosm)
        {
         if (prosm == null) throw new ArgumentNullException(nameof(prosm));
            _prosmMod = prosm;
        }

        public IModule _prosmMod;
    }
}
