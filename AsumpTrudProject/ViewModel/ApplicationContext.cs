﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using AsumpTrudProject.Model;

namespace AsumpTrudProject.ViewModel
{
   public class ApplicationContext:DbContext
    {
        public ApplicationContext():base("DefaultConnection")
        {

        }
        
        public DbSet<Trudizd> Trudizds { get; set; }

    }
}
