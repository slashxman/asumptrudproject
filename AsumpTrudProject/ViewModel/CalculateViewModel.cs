﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using AsumpTrudProject.Utils;

namespace AsumpTrudProject.ViewModel
{
    class CalculateViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
            
        public CalculateViewModel(IModule calc)
        {
            if (calc == null) throw new ArgumentNullException(nameof(calc));

            _CalcModule = calc;
        }

        //Fields
        private IModule _CalcModule;
    }
}
