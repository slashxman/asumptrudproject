﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AsumpTrudProject.Model
{
    public class Trudizd : INotifyPropertyChanged
    {
        private string vidrabot;
        private int sredtrud;

        public int Id { get; set; }

        public string Vidrabot
        {
            get { return vidrabot; }

            set
            {
                vidrabot = value;
                OnPropertyChanged("Vidrabot");

            }
        }

        public int Sredtrud
        {
            get { return sredtrud; }
            set
            {
                sredtrud = value;
                OnPropertyChanged("Sredtrud");
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
