﻿ using AsumpTrudProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AsumpTrudProject.View
{
    /// <summary>
    /// Interaction logic for TrudIzdDialogView.xaml
    /// </summary>
    public partial class TrudIzdDialogView : Window
    {
        public Trudizd Trudizd { get; private set; }

        public TrudIzdDialogView(Trudizd trud)
        {
            InitializeComponent();
            Trudizd= trud;
            this.DataContext =Trudizd;
           
        }
        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
