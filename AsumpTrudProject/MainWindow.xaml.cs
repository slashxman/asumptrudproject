﻿using AsumpTrudProject.Utils;
using AsumpTrudProject.View;
using AsumpTrudProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AsumpTrudProject
{

    /// <summary>
    /// Типы вьюшек для загрузки
    /// </summary>
    public enum ViewType
    {
        Home,
        VhodInf,
        SpravInf,
        Calculate,
        ProsmData

    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window , IModule
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;         
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            //загрузка вьюмодел для кнопок меню
            MenuViewModel vm = new MenuViewModel();
            //даем доступ к этому кодбихайнд
            vm.CodeBehind = this;
            //делаем эту вьюмодел контекстом данных
            this.DataContext = vm;

            //загрузка стартовой View
            LoadView(ViewType.Home);
        }

        public void LoadView(ViewType typeView)
        {
            switch (typeView)
            {
                case ViewType.Home:
                    //загружаем вьюшку, ее вьюмодель
                    HomeView view = new HomeView();
                    HomeWindowViewModel vm = new HomeWindowViewModel(this);
                    //связываем их м/собой
                    view.DataContext = vm;
                    //отображаем
                    this.OutputView.Content = view;
                    break;
                case ViewType.VhodInf:
                    VhodUcView vhod = new VhodUcView();
                   // VhodInfaViewModel vhodV = new VhodInfaViewModel(this);
                   // vhod.DataContext = vhodV;
                    this.OutputView.Content = vhod;
                    break;
                case ViewType.SpravInf:
                    SpravInfaView viewV = new SpravInfaView();
                    SpravInfaViewModel vmV = new SpravInfaViewModel(this);
                    viewV.DataContext = vmV;
                    this.OutputView.Content = viewV;
                    break;
                case ViewType.Calculate:
                    CalculateView viewC = new CalculateView();
                    CalculateViewModel vmC = new CalculateViewModel(this);
                    viewC.DataContext = vmC;
                    this.OutputView.Content = viewC;
                    break;
                case ViewType.ProsmData:
                    ProsmDataView viewP = new ProsmDataView();
                    ProsmDataViewModel vmP = new ProsmDataViewModel(this);
                    viewP.DataContext = vmP;
                   this.OutputView.Content = viewP;
                    break;
            } 


        }
        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }


        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonOpenMenu.Visibility = Visibility.Visible;
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
            gridContent.Margin = new Thickness(65, 65, 0, 0);
            
        }

        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
            ButtonCloseMenu.Visibility = Visibility.Visible;
            gridContent.Margin = new Thickness(210, 65, 0, 0);

        }

        private void MinimizeButton_Click(object sender, RoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
            
        }

        private void MaximizeButton_Click(object sender, RoutedEventArgs e)
        {

            SystemCommands.MaximizeWindow(this);
            ButtonMax.Visibility = Visibility.Collapsed;
            ButtonRestore.Visibility = Visibility.Visible;
           

        }

        private void RestoreButton_Click(object sender, RoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
            ButtonMax.Visibility = Visibility.Visible;
            ButtonRestore.Visibility = Visibility.Collapsed;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }
    }
}
