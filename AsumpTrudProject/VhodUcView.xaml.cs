﻿using AsumpTrudProject.Model;
using AsumpTrudProject.View;
using AsumpTrudProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AsumpTrudProject
{
    /// <summary>
    /// Interaction logic for VhodUcView.xaml
    /// </summary>
    public partial class VhodUcView : UserControl
    {
        ApplicationContext db;
        public VhodUcView()
        {
            InitializeComponent();
            db = new ApplicationContext();
            db.Trudizds.Load();
            this.DataContext = db.Trudizds.Local.ToBindingList();
        }

        // добавление
        private void Add_Click(object sender, RoutedEventArgs e)
        {
            TrudIzdDialogView dialogView = new TrudIzdDialogView(new Trudizd());
            if (dialogView.ShowDialog() == true)
            {
                Trudizd trudizd = dialogView.Trudizd;
                db.Trudizds.Add(trudizd);
                db.SaveChanges();
            }
        }

        // редактирование
        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            // если ни одного объекта не выделено, выходим
            if (phonesList.SelectedItem == null) return;
            // получаем выделенный объект
            Trudizd trudizd = phonesList.SelectedItem as Trudizd;

            TrudIzdDialogView dialogView = new TrudIzdDialogView(new Trudizd
            {
                Id = trudizd.Id,
                Vidrabot = trudizd.Vidrabot,
                Sredtrud = trudizd.Sredtrud
            });

            if (dialogView.ShowDialog() == true)
            {
                // получаем измененный объект
                trudizd = db.Trudizds.Find(dialogView.Trudizd.Id);
                if (trudizd != null)
                {
                    trudizd.Sredtrud = dialogView.Trudizd.Sredtrud;
                    trudizd.Vidrabot = dialogView.Trudizd.Vidrabot;
                    db.Entry(trudizd).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        // удаление
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            // если ни одного объекта не выделено, выходим
            if (phonesList.SelectedItem == null) return;
            // получаем выделенный объект
            Trudizd trudizd = phonesList.SelectedItem as Trudizd;
            db.Trudizds.Remove(trudizd);
            db.SaveChanges();
        }
    }
}
